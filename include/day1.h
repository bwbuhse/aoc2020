#ifndef __DAY1_H__
#define __DAY1_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "globals.h"

int uint32t_cmp(const void *a, const void *b);

int run1(void);

#ifdef __cplusplus
}
#endif

#endif /* __DAY1_H__ */
