#ifndef __DAY2_H__
#define __DAY2_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../include/globals.h"

typedef struct password {
  int min;
  int max;
  char rule;
  char *pw;
} password_t;

password_t *tokenize(password_t *password, char *input);
int isValid1(password_t *pw);
int isValid2(password_t *pw);

int run2();

#ifdef __cplusplus
}
#endif

#endif /* __DAY2_H__ */
