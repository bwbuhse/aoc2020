#ifndef __DAY3_H__
#define __DAY3_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../include/globals.h"

int run3();

#ifdef __cplusplus
}
#endif

#endif /* __DAY3_H__ */
