#include "../include/day1.h"

int GOAL = 2020;

int uint32t_cmp(const void *a, const void *b) {
  const uint32_t *ia = (const uint32_t *)a;
  const uint32_t *ib = (const uint32_t *)b;

  return (int)(*ia - *ib);
}

int run1() {
  FILE *fp = fopen("./input/day1-1.in", "r");
  // Check if file opened
  if (fp == NULL) {
    perror("Error while opening the input\n");
    return 1;
  }

  // Read all the numbers
  uint32_t input[BUFSIZE];
  int num_ints = 0;
  while (fscanf(fp, "%u", &input[num_ints++]) != EOF) {
  }
  num_ints -= 1;

  // Sort the input numbers
  qsort(input, num_ints, sizeof(uint32_t), uint32t_cmp);

  // Do the stuffs
  printf("Part 1\n");
  int done = 0;
  for (int i = 0; i < num_ints && !done; ++i) {
    for (int j = i + 1; j < num_ints; ++j) {
      int sum = input[i] + input[j];

      if (sum == GOAL) {
        printf("A: %u, B: %u, Sum: %u\n", input[i], input[j], sum);
        printf("Multiplicand: %u\n", input[i] * input[j]);
        done = 1;
        break;
      }
      if (sum > GOAL) {
        break;
      }
    }
  }

  printf("Part 2\n");
  for (int i = 0; i < num_ints; ++i) {
    for (int j = i + 1; j < num_ints; ++j) {
      for (int k = j + 1; k < num_ints; ++k) {
        int sum = input[i] + input[j] + input[k];

        if (sum == GOAL) {
          printf("A: %u, B: %u, C: %u, Sum: %u\n", input[i], input[j], input[k],
                 sum);
          printf("Multiplicand: %u\n", input[i] * input[j] * input[k]);
          return 0;
        }
        if (sum > GOAL) {
          break;
        }
      }
    }
  }

  return 0;
}
