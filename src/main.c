#include "../include/day1.h"
#include "../include/day2.h"
#include "../include/day3.h"

int main() {
  run1();
  run2();
  run3();

  return 0;
}
