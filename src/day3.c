#include "../include/day3.h"

// WRONG: 35, 26

int run3() {
  FILE *fp = fopen("./input/day3-1.in", "r");
  // Check if file opened
  if (fp == NULL) {
    perror("Error while opening the input\n");
    return 1;
  }

  // Allocate space for the map
  char **input = malloc(BUFSIZE * sizeof(char *));
  for (int i = 0; i < BUFSIZE; ++i) {
    input[i] = calloc(BUFSIZE, sizeof(char));
  }

  // Read the map in
  int height = -1, width = -1;
  while (fgets(input[++height], BUFSIZE, fp) != 0) {
  }
  // Subtract 1 to ignore newline char
  width = strlen(input[0]) - 1;

  /* for (int i = 0; i < height; ++i) { */
  /*   printf("%s\n", input[i]); */
  /* } */

  // Calculate number of trees hit
  // Part 1
  int numTrees = 0;
  for (int x = 0, y = 0; y < height; x += 3, y += 1) {
    char spot = input[y][x % width];
    if (spot == '#') {
      numTrees += 1;
    }
    /* printf("x: %d, y: %d, spot: %c, manual %c\n", y, x % width, spot, */
    /*        input[y][x % width]); */
  }
  printf("Part 1 - Number of trees hit: %d\n", numTrees);

  // Part 2
  int NUM_SLOPES = 5;
  int xSlopes[] = {1, 3, 5, 7, 1};
  int ySlopes[] = {1, 1, 1, 1, 2};
  unsigned long long totalTrees = 1;
  for (int i = 0; i < NUM_SLOPES; i++) {
    numTrees = 0;
    for (int x = 0, y = 0; y < height; x += xSlopes[i], y += ySlopes[i]) {
      char spot = input[y][x % width];
      if (spot == '#') {
        numTrees += 1;
      }
    }
    totalTrees *= numTrees;
  }
  
  printf("Part 2 - Number of trees hit multiplicand: %llu\n", totalTrees);

  // Free all malloced suff
  for (int i = 0; i < BUFSIZE; ++i) {
    free(input[i]);
  }
  free(input);

  return 0;
}
