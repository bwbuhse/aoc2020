#include "../include/day2.h"

#include <stdio.h>

password_t *tokenize(password_t *password, char *input) {
  int min, max;
  char *dashPtr, *colonPtr;

  // Find the dash
  dashPtr = strchr(input, '-');

  // Find the colon
  colonPtr = strchr(input, ':');

  min = strtol(input, NULL, 10);
  max = strtol(dashPtr + 1, NULL, 10);
  password->min = min;
  password->max = max;

  // Get the rule character
  // It's always the character before the colon
  password->rule = *(colonPtr - 1);
  password->pw = colonPtr + 2;

  return password;
}

int isValid1(password_t *password) {
  // Ensure password is valid
  if (!password || !password->pw) {
    return 0;
  }

  int count = 0;
  int len = strlen(password->pw);
  for (int i = 0; i < len; ++i) {
    if (password->pw[i] == password->rule) {
      ++count;
    }
  }

  if (count >= password->min && count <= password->max) {
    return 1;
  }
  return 0;
}

int isValid2(password_t *password) {
  // Ensure password is valid
  if (!password || !password->pw) {
    return 0;
  }

  int isValid = (password->pw[password->min - 1] == password->rule) ^
                (password->pw[password->max - 1] == password->rule);

  return isValid;
}

int run2() {
  FILE *fp = fopen("./input/day2-1.in", "r");
  // Check if file opened
  if (fp == NULL) {
    perror("Error while opening the input\n");
    return 1;
  }

  // Read all the numbers
  char *input = malloc(BUFSIZE * sizeof(char));
  int numValid1 = 0, numValid2 = 0;
  while (fgets(input, BUFSIZE, fp) != 0) {
    /* printf("%c\n", input[colonIdx]); */

    password_t *password = malloc(sizeof(password_t));
    tokenize(password, input);
    numValid1 += isValid1(password);
    numValid2 += isValid2(password);
    free(password);
  }
  printf("Num valid challenge 1: %d\n", numValid1);
  printf("Num valid challenge 2: %d\n", numValid2);

  free(input);

  return 0;
};
